#Adaptive learning rate for stochastic variational inference
#Implementation by Patrick Jähnichen based on Ranganath et.al.:"An Adaptive Learning Rate for Stochastic Variational Inference", ICML 2013
#Contact: patrick.jaehnichen@hu-berlin.de

"""
This method adapts the learning rate in stochastic variational inference so that the difference between
the new state after taking a noisy gradient step and a new state after taking a true gradient step is minimized.
To that end, we monitor the noisy gradient steps that are computed in a minibatch of data and use these to construct
an approximation to the true gradient via exponential moving averages.
As we are updating a global parameter in SVI and this parameter usually is a set of mixture components, we design the
algorithm to work with
"""
module AdaptiveLearning
using Distributions
type AdaptiveLearningRate
	tau
	moving_average::Array{Float64,1}
	# moving_variance::Array{Float64,1}
	moving_variance::Float64

	function AdaptiveLearningRate()
		new()
	end
	function AdaptiveLearningRate(noisy_gradient_samples::Array{Array{Float64,2},1}, gradient_step_covariance_traces::Array{Float64,1})
		# dimensions = size(noisy_gradient_samples)[1]
		# sampled_average = zeros(dimensions)
		# for i in 1:sample_count
		# 	sampled_average += rand(prior_distribution)
		# end
		# sampled_average ./= sample_count
		#random_gradient contains random gammas
		#normalize across topics
		# norm_grad = sum(random_gradient, 1)[:] ./ size(random_gradient)[1]
		# dirichlet_dist = Distributions.Dirichlet(norm_grad)
		# expectation = norm_grad ./ sum(norm_grad)
		# sampled_average = zeros(length(norm_grad))
		# for i in 1:sample_count
		# 	sampled_average += (rand(length(norm_grad)) - expectation).^2
		# end
		# sampled_average ./= sample_count
		# shuffled_gradient = deepcopy(random_gradient)
		# shuffleColumns!(shuffled_gradient)
		# gradient_sum = sum(shuffled_gradient[1:sample_count, :],1)[1:end]
		# gradient_sum /= sample_count
		sample_count = length(noisy_gradient_samples)
		# (K,V) = size(noisy_gradient_samples[1])
		D = length(vec(noisy_gradient_samples[1]))
		sampled_average = zeros(D)

		# MC estimate
		for i in 1:sample_count
			sampled_average += vec(noisy_gradient_samples[i])
		end
		sampled_average ./= sample_count

		# sampled_average = sum(noisy_gradient_samples, 1)[:] ./ size(noisy_gradient_samples)[1]
		sampled_variance = dot(sampled_average, sampled_average) + sum(gradient_step_covariance_traces)
		new(sample_count, sampled_average, sampled_variance)
	end
end

function shuffleColumns!(x::Array{Float64,2})
	(rows, cols) = size(x)
	perm = randperm(cols)
	for i=1:rows
		x[i,:] = permute!(x[i,:],perm)
	end
end

function adaptive_learning_rate(learning_rate_obj, gradient_step)
	# the sampled gradient is based on a collection of natural noisy gradients computed on the mini batch
	# compute new g_t
	# (K,V) = size(gradient_step)
	# h_t = zeros(K)
	# [h_t[k] = dot(gradient_step[k,:], gradient_step[k,:]) for k in 1:K]
	g_t = vec(gradient_step)
	h_t = dot(g_t, g_t)
	# for k in 1:K
	# 	g_t[k] = zeros(V)
	# 	updates = zeros(num_samples, V)
	# 	for i in 1:num_samples
	# 		g_t[k] += -previous_state + sampled_gradient[i][k,:]
	# 		updates[i,:] = sampled_gradient[i][k,:]
	# 	end
	# 	g_t[k] ./= num_samples
	# 	h_t[k] = dot(g_t[k], g_t[k]) + trace(cov(updates,1))
	# end
	# assume gradient is matrix mixture component dims x var_dims
	# mix_comp_dims = size(sampled_gradient)[1]
	# g_t = zeros(mix_comp_dims)
	# for i in 1:mix_comp_dims
	# 	g_t[i] = norm(vec(sampled_gradient[i,:]))/length(vec(sampled_gradient[i,:]))
	# 	# if i == 1
	# 	# 	println("g_t[i] = $(g_t[i]), sum grad: $(sum(sampled_gradient[i,:])), norm sum = $(sum(sampled_gradient[i,:])/length(vec(sampled_gradient[i,:])))")
	# 	# end
	# end
	# println("g_t norm: $(norm(g_t))")
	# println("g_t: $g_t")
	tau_rec = 1./learning_rate_obj.tau
	#recompute moving average
	# println("moving_average: $(size(learning_rate_obj.moving_average)), g_t: $(size(g_t))")
	new_moving_average = (1-tau_rec) .* learning_rate_obj.moving_average + tau_rec .* g_t
	# println("diff moving average: $(learning_rate_obj.moving_average - new_moving_average)")
	# println("new moving average norm: ", norm(new_moving_average))
	learning_rate_obj.moving_average = new_moving_average
	#recompute moving variance
	new_moving_var = (1-tau_rec) * learning_rate_obj.moving_variance + tau_rec * h_t
	# println("new moving variance: ", new_moving_var)
	learning_rate_obj.moving_variance = new_moving_var

	mov_av_dot_prod = dot(new_moving_average, new_moving_average)
	learning_rate = mov_av_dot_prod / new_moving_var
	# println("new learning rate: ", learning_rate)
	#reset tau
	new_tau = (1-learning_rate)*learning_rate_obj.tau + 1
	learning_rate_obj.tau = new_tau
	# println("new tau: ", learning_rate_obj.tau)

	learning_rate
end

export adaptive_learning_rate
end
